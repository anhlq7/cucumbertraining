#Author: anhlq12
Feature: Login

  Scenario: Login using the valid credentials
    Given The user is on login page
    When The user enters userid
    And The user enters password
    And The user clicks on login
    Then I expect that the user is navigated to homepage

  Scenario: User ID is blank
    Given The user is on login page
    When The user enters userid
    And The user enters password
    Then I expect that the system shows an error
