package com.anhlq12.cucumber.steps;

import java.util.HashMap;
import java.util.List;

import com.anhlq12.cucumber.constants.DataSourceEnum;
import com.anhlq12.cucumber.pages.LoginPage;
import com.anhlq12.cucumber.pages.ManagerHomePage;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class LoginDef {
	
	private LoginPage loginPage;
	private ManagerHomePage managerHomePage;
	private HashMap<Integer, List<String>> data;
	private String scenario;
	
	@Before
	public void before(Scenario s) {
	    this.scenario = s.getName();
	    data = loginPage.loadData(scenario);
	    System.out.println(data);
	}

	public LoginDef() {
		loginPage = new LoginPage();
		managerHomePage = new ManagerHomePage();
	}

	@Given("^The user is on login page$")
	public void gotoLoginPage() {
		loginPage.gotoLogin();
	}

	@When("^The user enters userid$")
	public void setUserName() {
		int index = DataSourceEnum.LoginPage.USERNAME.getIndex();
		loginPage.setUsername(data.get(0).get(index));
	}

	@When("^The user enters password$")
	public void setPassword() {
		int index = DataSourceEnum.LoginPage.PASSWORD.getIndex();
		loginPage.setPassword(data.get(0).get(index));
	}

	@When("^The user clicks on login$")
	public void click() {
		loginPage.clickLogin();
	}
	
	@Then("^I expect that the user is navigated to homepage$")
	public void verifyPage() {
		Assert.assertTrue(managerHomePage.isInManagerHomepage());
	}
	
	@Then("^I expect that the system shows an error$")
	public void verifyError() {
		int index = DataSourceEnum.LoginPage.ERROR_MESSAGE.getIndex();
		String expectError = data.get(0).get(index);
		String actualError = loginPage.getUserBlankError();
		Assert.assertEquals(expectError, actualError);
	}
}
