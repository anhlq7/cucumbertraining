package com.anhlq12.cucumber;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
 features = "src/test/java/com/anhlq12/cucumber/features",
 glue= {"src/test/java/com/anhlq12/cucumber/steps/"},
plugin = { "pretty", "html:target/cucumber-reports" },
 monochrome = true
 )
public class TestRunner {}
