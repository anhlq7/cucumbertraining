package com.anhlq12.cucumber.pages;

import java.util.Map;

import org.openqa.selenium.By;

import com.anhlq12.cucumber.libs.WebDriverUtils;

public class ManagerHomePage extends Page {
	private WebDriverUtils webUtils = new WebDriverUtils();
	private Map<String, By> elements = getElements(this.getClass().getSimpleName());

	public ManagerHomePage() {
		super();
	}

	public boolean isInManagerHomepage() {
		return webUtils.waitForAndGetElement(elements.get("ManagerId")) != null;
	}
}
