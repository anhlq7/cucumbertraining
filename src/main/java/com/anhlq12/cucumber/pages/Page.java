package com.anhlq12.cucumber.pages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.openqa.selenium.By;
import com.anhlq12.cucumber.constants.Constant;
import com.anhlq12.cucumber.constants.RepoEnum;
import com.anhlq12.cucumber.libs.ExcelUtil;
import com.anhlq12.cucumber.libs.Utils;

public class Page {

	private ExcelUtil excelUtil;
	private Properties p;

	public Page() {
		p = Utils.loadProperties(Constant.DIR_CONFIG);
		excelUtil = new ExcelUtil(Constant.DIR_OBJECT_REPO + "\\" + p.getProperty("object_repo_filename"));
	}

	public Map<String, By> getElements(String sheetName) {
		System.out.println("sheetName"+sheetName);
		Map<String, By> elements = new HashMap<String, By>();
		int indName = RepoEnum.NAME.getIndex();
		int indLocator = RepoEnum.LOCATOR.getIndex();
		int indType = RepoEnum.TYPE.getIndex();
		Map<Integer, List<String>> hm = excelUtil.read(sheetName);
		// start from 1 (row = 0: header)
		for (int i = 1; i < hm.keySet().size(); i++) {
			List<String> values = hm.get(i);
			String name = values.get(indName);
			String type = values.get(indType);
			String locator = values.get(indLocator);
			elements.put(name, getElement(locator, type));
		}
		return elements;
	}

	public By getElement(String locator, String type) {
		type = type.toLowerCase();
		if ("cssselector".equals(type)) {
			return By.cssSelector(locator);
		} else if ("xpath".equals(type)) {
			return By.xpath(locator);
		}
		return null;
	}
}
