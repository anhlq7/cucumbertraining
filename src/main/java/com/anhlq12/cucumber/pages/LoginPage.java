package com.anhlq12.cucumber.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.By;

import com.anhlq12.cucumber.constants.Constant;
import com.anhlq12.cucumber.constants.DataSourceEnum;
import com.anhlq12.cucumber.libs.ExcelUtil;
import com.anhlq12.cucumber.libs.WebDriverUtils;

public class LoginPage extends Page{

	private WebDriverUtils webUtils = new WebDriverUtils();
	private Map<String, By> elements = getElements(this.getClass().getSimpleName());

	public LoginPage() {
		super();
	}

	public void gotoLogin() {
		webUtils.navigateTo(Constant.URLS.get("login"));
	}

	public void setUsername(String username) {
		webUtils.sendKeys(elements.get("userid"), username);
	}

	public void setPassword(String password) {
		webUtils.sendKeys(elements.get("password"), password);
	}
	
	public String getUserBlankError() {
		return webUtils.getText(elements.get("msg_user_blank"));
	}

	public void clickLogin() {
		webUtils.click(elements.get("login"));
	}

	public HashMap<Integer, List<String>> loadData(String key) {
		String sheetName = this.getClass().getSimpleName();
		HashMap<Integer, List<String>> data = new HashMap<Integer, List<String>>();
		ExcelUtil excelUtil = new ExcelUtil(Constant.DIR_TEST_DATA);
		int keyStartCol = DataSourceEnum.LoginPage.KEY_START.getIndex();
		int keyEndCol = DataSourceEnum.LoginPage.KEY_END.getIndex();
		int keyStartRow = -1;
		int keyEndRow = -1;
		int lastRow = excelUtil.getLastRowNum(sheetName);
		for(int row = 0; row <= lastRow; row++) {
			String v = excelUtil.getCellValue(sheetName, row, keyStartCol);
			if(v != null && key.equalsIgnoreCase(v)) {
				keyStartRow = row;
				break;
			}
		}

		for(int row = 0; row <= lastRow; row++) {
			String v = excelUtil.getCellValue(sheetName, row, keyEndCol);
			if(v != null && key.equalsIgnoreCase(v)) {
				keyEndRow = row;
				break;
			}
		}
		System.out.println("keyStartRow"+keyStartRow);
		System.out.println("keyEndRow"+keyEndRow);
		int count = 0;
		for(int i = keyStartRow + 2; i < keyEndRow; i++) {
			List<String> rowData = new ArrayList<String>();
			//add to have the same index in excel when getting data
			rowData.add("");
			for(int j = keyStartCol + 1; j < keyEndCol; j++) {
				String cellValue = excelUtil.getCellValue(sheetName, i, j);
				rowData.add(cellValue);
			}
			data.put(count, rowData);
			count++;
		}
		return data;
	}
}
