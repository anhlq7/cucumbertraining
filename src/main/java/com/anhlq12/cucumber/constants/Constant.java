package com.anhlq12.cucumber.constants;

import java.util.HashMap;

public class Constant {
	
	public static final String DIR_PROJECT = System.getProperty("user.dir");

	public static final String DIR_OBJECT_REPO = DIR_PROJECT + "\\" + "object_repository";
	
	public static final String DIR_TEST_DATA = DIR_PROJECT + "\\" + "testdata\\test_data.xlsx";

	public static final String DIR_CONFIG = DIR_PROJECT + "\\" + "config.properties";
	
	public static final HashMap<String, String> URLS = 
			new HashMap<String, String>() {{
				put("login", "http://demo.guru99.com/V4/");
				put("homepage", "http://demo.guru99.com/V4/manager/Managerhomepage.php");
	}};

}
