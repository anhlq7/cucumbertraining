package com.anhlq12.cucumber.constants;

public class DataSourceEnum {

	public enum LoginPage {
		KEY_START(0),
		USERNAME(1),
		PASSWORD(2),
		ERROR_MESSAGE(3),
		KEY_END(4);
		int index;
		private LoginPage(int index) {
			this.index = index;
		}
		public int getIndex() {
			return index;
		}
	}
	
}
