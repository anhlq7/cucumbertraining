package com.anhlq12.cucumber.constants;


public enum RepoEnum {
	NAME(0),
	LOCATOR(1),
	TYPE(2);
	int index;
	private RepoEnum(int index) {
		this.index = index;
	}
	public int getIndex() {
		return index;
	}
}

