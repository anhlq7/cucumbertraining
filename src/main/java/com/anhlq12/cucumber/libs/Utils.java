package com.anhlq12.cucumber.libs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import com.anhlq12.cucumber.constants.Constant;
import com.anhlq12.cucumber.constants.DataSourceEnum;

public class Utils {
	
	public static Properties loadProperties(String path) {
		FileInputStream fis;
		Properties p = new Properties();
		try {
			fis = new FileInputStream(path);
			p.load(fis);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}
}
