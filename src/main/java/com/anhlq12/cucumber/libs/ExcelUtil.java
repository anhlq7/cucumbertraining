package com.anhlq12.cucumber.libs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
		private FileInputStream fis;
		private Workbook workbook;

		public ExcelUtil(String pathname) {
			try {
				fis = new FileInputStream(new File(pathname));
				workbook = new XSSFWorkbook(fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public List<String> getDataByRow(String sheetName, int rowNo) {
			List<String> values = new ArrayList<String>();
			Sheet sheet = workbook.getSheet(sheetName);
			if(sheet == null) {
				return null;
			}
			//first row. start index = 0
			Row row = sheet.getRow(rowNo);
			if(row == null) {
				return null;
			}
			for(Cell cell : row) {
				if(CellType.STRING.equals(cell.getCellType())) {
					String v = cell.getStringCellValue();
					values.add(v);
				}
			}
			return values;
		}

		public Map<Integer, List<String>> read(String sheetname) {
			Sheet sheet = workbook.getSheet(sheetname);
			Map<Integer, List<String>> hm = new HashMap<Integer, List<String>>();
			int key = 0;
			for (Row row : sheet) {
				List<String> values = new ArrayList<String>();
				for (Cell cell : row) {
					switch (cell.getCellTypeEnum()) {
					case STRING:
						String v = cell.getStringCellValue();
						values.add(v);
						break;
					case NUMERIC:
						break;
					case BOOLEAN:
						break;
					case FORMULA:
						break;
					}
				}
				hm.put(key, values);
				key++;
			}
			return hm;
		}
		
		public String getCellValue(String sheetName, int row, int column) {
			Sheet sheet = workbook.getSheet(sheetName);
			if(sheet == null) {
				return null;
			}
			Cell cell = sheet.getRow(row).getCell(column);
			if(cell == null) {
				return null;
			}
			return cell.getStringCellValue();
		}
		
		public int getLastRowNum(String sheetName) {
			Sheet sheet = workbook.getSheet(sheetName);
			if(sheet == null) {
				return -1;
			}
			return sheet.getLastRowNum();
		}
}
