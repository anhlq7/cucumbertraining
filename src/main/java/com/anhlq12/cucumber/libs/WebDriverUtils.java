package com.anhlq12.cucumber.libs;

import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anhlq12.cucumber.constants.Constant;

public class WebDriverUtils {

	private static Properties p = Utils.loadProperties(Constant.DIR_CONFIG);;
	private static WebDriver driver = getBrowser(p.getProperty("browser"));
	private static final int DEFAULT_TIME_OUT = Integer.parseInt(p.getProperty("DEFAULT_TIME_OUT"));
	private static final int DEFAULT_POLLING_INTERVAL = Integer.parseInt(p.getProperty("DEFAULT_POLLING_INTERVAL"));
	
	public static WebDriver getBrowser(String browserName) {
		browserName = browserName.toLowerCase().trim();
		if ("chrome".equals(browserName)) {
			System.setProperty("webdriver.chrome.driver", Constant.DIR_PROJECT + "\\" + p.getProperty("chrome_driver"));
			driver = new ChromeDriver();
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return driver;
	}

	public WebElement waitForAndGetElement(final By by) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(DEFAULT_TIME_OUT))
				.pollingEvery(Duration.ofSeconds(DEFAULT_POLLING_INTERVAL)).ignoring(NoSuchElementException.class);
		try {
			WebElement e = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(by);
				}
			});
			return e;
		} catch (TimeoutException e) {
			return null;
		}
	}
	
	public void navigateTo(String url) {
		driver.get(url);
	}
	
	public boolean click(By by) {
		WebElement element = waitForAndGetElement(by);
		if(element == null) {
			return false;
		}
		WebDriverWait wait = new WebDriverWait(driver,30);
		boolean b = (wait.until(ExpectedConditions.elementToBeClickable(element)) != null);
		if(!b) {
			return false;
		}
		element.click();
		return true;
	}
	
	public boolean sendKeys(By by, String keysToSend) {
		WebElement element = waitForAndGetElement(by);
		if(element == null) {
			return false;
		}
		try {
			element.sendKeys(keysToSend);
			return true;
		}catch(IllegalArgumentException e) {
			return false;
		}
	}
	
	public String getText(By by) {
		WebElement element = waitForAndGetElement(by);
		if(element == null) {
			return null;
		}
		return element.getText();
	}
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public void closeBrowser() {
		driver.close();
	}
}
